/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Jhon
 */
public class ManejoArchivos {
    private String ruta;

    public ManejoArchivos() {
        obtenerRutaPrincipal();
    }

    public String getRuta() {
        return ruta;
    }
    
    private void obtenerRutaPrincipal() {
        ruta = "";
        JFileChooser archivo = new JFileChooser();
        archivo.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int autentico = archivo.showOpenDialog(null);
        if (autentico == JFileChooser.APPROVE_OPTION) {
            ruta = archivo.getSelectedFile().getPath();
        }
        if (ruta.startsWith(System.getenv("USERPROFILE") + "\\Documents\\") || ruta.startsWith(System.getenv("USERPROFILE") + "\\Downloads\\")|| 
                ruta.startsWith(System.getenv("USERPROFILE") + "\\Desktop\\") || !ruta.startsWith(System.getenv("HOMEDRIVE"))) {
        //Rutas correctas    
        } else if (ruta.startsWith(System.getenv("HOMEDRIVE"))){
            JOptionPane.showMessageDialog(null, "<html>Esta carpeta no es permitida<br>debe elegir una carpeta diferente<br>" + ruta, "ruta incorrecta", JOptionPane.ERROR_MESSAGE);
            ruta = "";
        }
    }
    
    public String[] lectorContenido() {
        String info [] = null; 
        if (!ruta.isEmpty()) {
            File lector = new File(ruta);
            info = lector.list();
        } else {
            JOptionPane.showMessageDialog(null, "No existen sub-carpetas para ingresar");
        }
        return info;
    }
    
    public void moverArchivos() {
        String [] datos = lectorContenido();
        if (datos != null) {
            Path destino = Paths.get(ruta);
            for (int i = 0; i < datos.length; i++) {
                String rutaCompleta = ruta + "\\" + datos[i];
                String[] archivos = new File(rutaCompleta).list();
                if (archivos != null) {
                        for (int j = 0; j < archivos.length; j++) {
                        if (archivos[j].contains(".")) {
                            Path origen = Paths.get(rutaCompleta + "\\" + archivos[j]);
                            try {
                                Files.move(origen, destino.resolve(origen.getFileName()));
                            } catch (IOException ex) {
                                Logger.getLogger(ManejoArchivos.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                } 
            }
        }
        JOptionPane.showMessageDialog(null, "Operación terminada");
        ruta = "";
    }
    
    public void renombrarArchivos(String nombre) {
        nombre = nombre.trim();
        String [] archivos = lectorContenido();
        for (int i = 0; i < archivos.length; i++) {
            if (archivos[i].contains(".")) {
                File archivoSinRenombrar = new File(ruta + "\\" + archivos[i]); 
                File archivoNombreFinal;
                String [] nombreOriginal = archivos[i].split("\\.");
                try {
                    Integer.parseInt(nombreOriginal[0].replaceAll("[^0-9]", ""));
                    archivoNombreFinal = new File(ruta + "\\" + nombre + " - " + nombreOriginal[0].replaceAll("[A-Za-z]* *-*_*\\[*\\]*", "") + "." + nombreOriginal[1]);
                    archivoSinRenombrar.renameTo(archivoNombreFinal);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        JOptionPane.showMessageDialog(null, "Operación terminada");
    }
}
